﻿namespace csharp2_lesson2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tofahrenheitbutton = new System.Windows.Forms.Button();
            this.celsiustextbox = new System.Windows.Forms.TextBox();
            this.fahrenheittextbox = new System.Windows.Forms.TextBox();
            this.celsiuslabel = new System.Windows.Forms.Label();
            this.fahrenheitlabel = new System.Windows.Forms.Label();
            this.tocelsiusbutton = new System.Windows.Forms.Button();
            this.degreelabel = new System.Windows.Forms.Label();
            this.Answer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tofahrenheitbutton
            // 
            this.tofahrenheitbutton.Location = new System.Drawing.Point(91, 186);
            this.tofahrenheitbutton.Name = "tofahrenheitbutton";
            this.tofahrenheitbutton.Size = new System.Drawing.Size(130, 42);
            this.tofahrenheitbutton.TabIndex = 0;
            this.tofahrenheitbutton.Text = "To Fahrenheit";
            this.tofahrenheitbutton.UseVisualStyleBackColor = true;
            this.tofahrenheitbutton.Click += new System.EventHandler(this.tocbutton_Click);
            // 
            // celsiustextbox
            // 
            this.celsiustextbox.Location = new System.Drawing.Point(139, 28);
            this.celsiustextbox.Name = "celsiustextbox";
            this.celsiustextbox.Size = new System.Drawing.Size(132, 20);
            this.celsiustextbox.TabIndex = 1;
            // 
            // fahrenheittextbox
            // 
            this.fahrenheittextbox.Location = new System.Drawing.Point(140, 129);
            this.fahrenheittextbox.Name = "fahrenheittextbox";
            this.fahrenheittextbox.Size = new System.Drawing.Size(131, 20);
            this.fahrenheittextbox.TabIndex = 2;
            // 
            // celsiuslabel
            // 
            this.celsiuslabel.AutoSize = true;
            this.celsiuslabel.Location = new System.Drawing.Point(44, 35);
            this.celsiuslabel.Name = "celsiuslabel";
            this.celsiuslabel.Size = new System.Drawing.Size(57, 13);
            this.celsiuslabel.TabIndex = 3;
            this.celsiuslabel.Text = "Fahrenheit";
            // 
            // fahrenheitlabel
            // 
            this.fahrenheitlabel.AutoSize = true;
            this.fahrenheitlabel.Location = new System.Drawing.Point(43, 136);
            this.fahrenheitlabel.Name = "fahrenheitlabel";
            this.fahrenheitlabel.Size = new System.Drawing.Size(40, 13);
            this.fahrenheitlabel.TabIndex = 4;
            this.fahrenheitlabel.Text = "Celsius";
            // 
            // tocelsiusbutton
            // 
            this.tocelsiusbutton.Location = new System.Drawing.Point(91, 67);
            this.tocelsiusbutton.Name = "tocelsiusbutton";
            this.tocelsiusbutton.Size = new System.Drawing.Size(130, 38);
            this.tocelsiusbutton.TabIndex = 5;
            this.tocelsiusbutton.Text = "To Celsius";
            this.tocelsiusbutton.UseVisualStyleBackColor = true;
            this.tocelsiusbutton.Click += new System.EventHandler(this.tofbutton_Click);
            // 
            // degreelabel
            // 
            this.degreelabel.AutoSize = true;
            this.degreelabel.Location = new System.Drawing.Point(166, 274);
            this.degreelabel.Name = "degreelabel";
            this.degreelabel.Size = new System.Drawing.Size(0, 13);
            this.degreelabel.TabIndex = 6;
            // 
            // Answer
            // 
            this.Answer.AutoSize = true;
            this.Answer.Location = new System.Drawing.Point(103, 274);
            this.Answer.Name = "Answer";
            this.Answer.Size = new System.Drawing.Size(42, 13);
            this.Answer.TabIndex = 7;
            this.Answer.Text = "Answer";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 393);
            this.Controls.Add(this.Answer);
            this.Controls.Add(this.degreelabel);
            this.Controls.Add(this.tocelsiusbutton);
            this.Controls.Add(this.fahrenheitlabel);
            this.Controls.Add(this.celsiuslabel);
            this.Controls.Add(this.fahrenheittextbox);
            this.Controls.Add(this.celsiustextbox);
            this.Controls.Add(this.tofahrenheitbutton);
            this.Name = "Form1";
            this.Text = "Tenperature Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tofahrenheitbutton;
        private System.Windows.Forms.TextBox celsiustextbox;
        private System.Windows.Forms.TextBox fahrenheittextbox;
        private System.Windows.Forms.Label celsiuslabel;
        private System.Windows.Forms.Label fahrenheitlabel;
        private System.Windows.Forms.Button tocelsiusbutton;
        private System.Windows.Forms.Label degreelabel;
        private System.Windows.Forms.Label Answer;
    }
}

