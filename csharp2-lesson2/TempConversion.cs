﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp2_lesson2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void tofbutton_Click(object sender, EventArgs e)
        {
            double cel = Convert.ToDouble(celsiustextbox.Text);
            string faconverted = Convert.ToString((cel - 32)*5/9);
            degreelabel.Text = faconverted+" C";
        }

        private void tocbutton_Click(object sender, EventArgs e)
        {
            double fa = Convert.ToDouble(fahrenheittextbox.Text);
            string celconverted = Convert.ToString(fa*9/5+32);
            degreelabel.Text = celconverted+" F";
        }
    }
}
